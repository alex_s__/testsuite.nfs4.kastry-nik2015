__name__='TestSuite'
import tests
import os
import shutil
import traceback
from sets import Set
import itertools


NFS4_FOLDER = 'mounted_folder/share1/test_env/'

def printd(*debugoutput):
    if os.getenv('DEBUG') == 'True':
        string = ''
        for argument in debugoutput:
            string = string + str(argument)
        print('DEBUG: ' + string)

class Environment:
    """
    class prepares an evironment folder for test files inside nfs4 mountpoint
    """
    def __init__(self, nfs_folder):
        printd('current working directory ', os.getcwd())
        self.set_nfs_folder(nfs_folder)
        self.create_directory()


    def set_nfs_folder(self, path):
        self.nfs_folder=path
        printd('nfs4 environment folder has been set to ', self.nfs_folder)


    def create_directory(self):
        os.mkdir(self.nfs_folder)
        printd('directory ', self.nfs_folder, ' created')
        """
        according to https://docs.python.org/2/library/os.html#files-and-directories - On some systems, mode is ignored, and on systems where it is not ignored the current umask valued is masked out.
        so we are forced to use os.chmod
        """
        os.chmod(self.nfs_folder, 0777)

    def create_file(self, testcasename):
        self.testfile = open(self.nfs_folder+testcasename, 'w')
        self.testfile.write(testcasename)
        self.testfile.close()
        printd('test file created: ', testcasename)
        return self.testfile

    def get_testfile(self):
        return self.testfile


    def destroy_environment(self):
        shutil.rmtree(self.nfs_folder)
        printd('envorment ', self.nfs_folder, ' destroyed')


    """
        def to_dict:
            return dict
    """




def printd_imported_testcases():
    printd('variable tests.__all__: ',tests.__all__)
    printd('dictionary of loaded modules to work with tests.modules2:',tests.modules2)
    printd('function tests.modules2.viewitems(): ', tests.modules2.viewitems())

def run_test_suite():
    """
    Iteration throught test cases
    """
    Env = Environment(NFS4_FOLDER)
    printd('iterating test cases')
    try:
        for name, TC in tests.modules2.viewitems():
#            printd(TC, dir(TC))
            getattr(TC, name)(Environment=Env) #Initialize testcase class and run it
    except Exception as inst:
        print('we have exception')
        traceback.print_exc()
    finally:
        Env.destroy_environment()


run_test_suite()


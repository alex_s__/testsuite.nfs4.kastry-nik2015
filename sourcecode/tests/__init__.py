from os.path import dirname, basename, isfile
import glob
import sys
"""getting list of files from current directory by mask"""
modules1 = glob.glob(dirname(__file__)+"/TC*.py")
"""
inserting each python file in to __all__ variable
so statement
from sourcecode.tests import *
will imported all files in tests folder as well.
"""
__all__ = [ basename(f)[:-3] for f in modules1 if isfile(f)]


"""create a dictionary of Test Cases"""
modules2 = { }
for module in __all__:
    __import__( "tests."+module)
    modules2[ module ] = sys.modules[ 'tests.'+module]
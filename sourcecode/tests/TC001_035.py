import os
from sets import Set
import itertools
import logging


class TC001_035:


    def __init__(self, Environment):
        """
        TC001-064
        test various chmods on files
        """

        self.testcasename = 'TC0'
        self.permissions_v1 = {'action':os.chmod, 'S_IRUSR':0400, 'S_IWUSR':0200, 'S_IXUSR':0100}
        self.printd('TESTCASE '+self.testcasename+' INITIALIZED!!!')
        self.action = os.chmod
        self.Env = Environment
        logging.basicConfig(filename='logs/TestSuite.log',level=logging.INFO)


        """generating a Set of permissions"""
        self.permissions_v2 = Set()
        for x in itertools.combinations('1234567', 3):
            self.permissions_v2.add(x)
#        print(self.permissions_v2, len(self.permissions_v2))
        """iterating through permissions and running for each test case action"""
        for idx,permission in enumerate(self.permissions_v2):
            permission_v3 = int("0"+"".join(permission), 8)#"0"+"".join(permission)
            self.printd(idx+1, permission_v3, type(permission_v3))
            self.testfile = self.Env.create_file(self.testcasename+str(idx+1))
            if self.run_testcase(action=self.permissions_v1['action'], set_parameters=permission_v3):
                print(self.testcasename+str(idx+1)+': Passed')
                logging.info(self.testcasename+str(idx+1)+': Passed')
            else:
                print(self.testcasename+str(idx+1)+': Failed')
                self.testcasename+str(idx+1)+': Failed'

    def get_actual_result(self):
        return(oct(os.stat(self.testfile.name).st_mode))


    def printd(self, *debugoutput):
        if os.getenv('DEBUG') == 'True':
            string = ''
            for argument in debugoutput:
                string = string + str(argument)
            print('DEBUG: ' + string)

    def check_privilegies(self):
        pass

    def run_testcase(self, action, set_parameters):
        self.printd('testing ' ,oct(set_parameters) ,' permissions on file '+self.testfile.name)
        self.printd('before ', self.get_actual_result()[-4:])
        action(self.testfile.name, set_parameters)
        self.printd('after ', self.get_actual_result()[-4:])
        if self.get_actual_result()[-4:] == oct(set_parameters):
            return True
        else:
            return False







import os
import logging

class TC037:
    def __init__(self, Environment):
        """
        Test content modification possibility
        :param Environment:
        :return:
        """
        self.testcasename = 'TC037'
        self.Env = Environment
        self.content = {} #todo
        self.action = ''
        self.expected_result = self.testcasename
        self.testfile = self.Env.create_file(self.testcasename)
        self.printd('TESTCASE '+self.testcasename+' INITIALIZED!!!')
        logging.basicConfig(filename='logs/TestSuite.log',level=logging.INFO)


        if self.run_testcase():
           print(self.testcasename+': Passed')
           logging.info(self.testcasename+': Passed')

        else:
           print(self.testcasename+': Failed')
           self.testcasename+': Failed'



    def get_actual_result(self):
        with open(self.testfile.name) as f:
            return f.readlines()[0]



    def run_testcase(self):
        self.printd('contents of file: ',self.get_actual_result())
        if self.expected_result == self.get_actual_result():
            return True
        else:
            return False

    def printd(self, *debugoutput):
        if os.getenv('DEBUG') == 'True':
            string = ''
            for argument in debugoutput:
                string = string + str(argument)
            print('DEBUG: ' + string)
